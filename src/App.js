import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import './App.css'
// import List from './List'
// import Timer from './Timer.js'
// import { FlatList, ActivityIndicator, Text, View  } from 'react';

//state
class App extends Component {
  constructor(props){
    super(props);
    this.state ={ 
      id:"",
      name:"",
      price:"",
      image:"",
      data: {},
      isLoading: false
    }
  }

  componentDidMount(){
    
  }


  handleGet = (e) => {
    e.preventDefault()

    if(this.state.id == ""){
      alert("Id harus diisi");
    }else{
      //method get
      fetch('http://localhost:4000/api/products/'+this.state.id)
        .then((response) => response.json())
        .then((data) => {

          this.setState({
            data: data,
            isLoading: false
          });

          console.log(data);
        })
        .catch((error) =>{
          console.error(error);
        });
    }
  }

  handleInsert = (e) => {
    e.preventDefault()

    // console.log(this.state);
    if(this.state.name == ""){
      alert("nama harus diisi");
    }else if(this.state.price == ""){
      alert("price harus diisi");
    }else if(this.state.image == ""){
      alert("image harus diisi");
    }else{
      //method post
      fetch('http://localhost:4000/api/products', {
         method: 'post',
         headers: {'Content-Type':'application/json'},
         body: JSON.stringify({
          "name": this.state.name,
          "price": this.state.price,
          "image": this.state.image
         })
      })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
      })
      .catch((error) =>{
        console.error(error);
      });
    }
  }

  handleUpdate = (e) => {
    e.preventDefault()

    // console.log(this.state);
    if(this.state.id == ""){
      alert("Id harus diisi");
    }else if(this.state.name == ""){
      alert("nama harus diisi");
    }else if(this.state.price == ""){
      alert("price harus diisi");
    }else if(this.state.image == ""){
      alert("image harus diisi");
    }else{
      //method put
      fetch('http://localhost:4000/api/products/'+this.state.id, {
         method: 'put',
         headers: {'Content-Type':'application/json'},
         body: JSON.stringify({
          "name": this.state.name,
          "price": this.state.price,
          "image": this.state.image
         })
      })
      .then((response) => response.json())
      .then((data) => {

        this.setState({
          data: data,
          isLoading: false
        });

        console.log(data);
      })
      .catch((error) =>{
        console.error(error);
      });
    }
  }

  handleDelete = (e) => {
    e.preventDefault()

    if(this.state.id == ""){
      alert("Id harus diisi");
    }else{
      //method delete
      // alert('http://localhost:4000/api/products/'+this.state.id);
      fetch('http://localhost:4000/api/products/'+this.state.id, {
         method: 'delete',
         headers: {'Content-Type':'application/json'},
         body: ''
      })
      .then((response) => response.json())
      .then((data) => {

        this.setState({
          data: data,
          isLoading: false
        });

        console.log(data);
      })
      .catch((error) =>{
        console.error(error);
      });
    }
  }

  handleId = (e) =>{
    this.setState({
      id: e.target.value
    })

    console.log(this.state);
  }

  handleName = (e) =>{
    this.setState({
      name: e.target.value
    })

    console.log(this.state);
  }

  handlePrice = (e) =>{
    this.setState({
      price: e.target.value
    })

    console.log(this.state);
  }

  handleImage = (e) =>{
    this.setState({
      image: e.target.value
    })

    console.log(this.state);
  }

  render(){
    const {items, isLoading} = this.state

    if(isLoading){
      return <p>Loading.......</p>
    }
    return(
      <div className="App">
        <button onClick={this.handleGet}>Get All Product</button>
        <form>
          <span>Insert Product</span>
          <input id="id" value={this.state.id} onChange={this.handleId} placeholder="Id"/>
          <input id="name" value={this.state.name} onChange={this.handleName} placeholder="Product Name"/>
          <input id="price" value={this.state.price} onChange={this.handlePrice} placeholder="Price"/>
          <input id="image" value={this.state.image} onChange={this.handleImage} placeholder="Image url"/>

          <button onClick={this.handleInsert}>Insert</button>
          <button onClick={this.handleUpdate}>Update</button>
          <button onClick={this.handleDelete}>Delete</button>
        </form>

        {/*<form>
          <span>Update Product</span>
          <input id="id" value={this.state.id} onChange={this.handleId} placeholder="Id"/>
          <input id="name" value={this.state.name} onChange={this.handleName} placeholder="Product Name"/>
          <input id="price" value={this.state.price} onChange={this.handlePrice} placeholder="Price"/>
          <input id="image" value={this.state.image} onChange={this.handleImage} placeholder="Image url"/>

          <button onClick={this.handleUpdate}>Update</button>
        </form>*/}
      </div>
    );
  }
}
export default App